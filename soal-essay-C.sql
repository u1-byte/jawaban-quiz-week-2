create table customers (
    id int auto_increment,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    primary key(id)
);

create table orders (
    id int auto_increment,
    amount int,
    customer_id int,
    primary key(id),
    foreign key(customer_id) REFERENCES customers(id)
);