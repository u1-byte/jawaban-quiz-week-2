<?php
    function perolehan_medali($array){
        $res = array();
        $emas = 0;
        $perak = 0;
        $perunggu = 0;
        foreach ($array as $arr){
            if ($arr[1] == 'emas'){
                $emas += 1;
            }
            elseif ($arr[1] == 'perak'){
                $perak += 1;
            }
            elseif ($arr[1] == 'perunggu'){
                $perunggu += 1;
            }
        }
    }

    //TEST CASES
    print_r(perolehan_medali(
        array(
            array('Indonesia', 'emas'),
            array('India', 'perak'),
            array('Korea Selatan', 'emas'),
            array('India', 'perak'),
            array('India', 'emas'),
            array('Indonesia', 'perak'),
            array('Indonesia', 'emas'),
        )
        ));
?>