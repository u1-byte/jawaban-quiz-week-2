<?php
    function hitung($string_data){
        $operator = "";
        $bil1 = "";
        $bil2 = "";
        $toggle = 0;
        $split = str_split($string_data);
        foreach ($split as $char){
            if ($char == '*' || $char =='+' || $char =='-' || $char ==':' || $char =='%'){
                $toggle = 1;
                $operator .= $char;
            }
            else{
                if ($toggle == 0) {
                    $bil1 .= $char;
                }
                elseif ($toggle == 1) {
                    $bil2 .= $char;
                }
            }
        }
        $bil1 = (int)$bil1;
        $bil2 = (int)$bil2;
        if ($operator == '+'){
            return $bil1 + $bil2 . "<br>";
        }
        elseif ($operator == '-'){
            return $bil1 - $bil2 . "<br>";
        }
        elseif ($operator == ':'){
            return $bil1 / $bil2 . "<br>";
        }
        elseif ($operator == '*'){
            return $bil1 * $bil2 . "<br>";
        }
        elseif ($operator == '%'){
            return $bil1 % $bil2 . "<br>";
        }
    }

    //TEST CASES
    echo hitung("102*2"); //204
    echo hitung("2+3"); //5
    echo hitung("100:25"); //4
    echo hitung("10%2"); //0
    echo hitung("99-2"); //97
?>