select c.name as customer_name, sum(o.amount) as total_amount
from customers c inner join orders o
on c.id = o.customer_id
group by customer_name
order by total_amount asc;